package reflection.tester;




import java.lang.reflect.Method;
import java.util.ArrayList;

import java.util.List;



public class TestRunner {
    public List<Method> methodsList = new ArrayList<>();
    public void runTests(List<String> testClassNames) {
        for (String testClassName : testClassNames) {
            Class<?> aClass;
            try {
                aClass = Class.forName(testClassName);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }


            Method[] methods = aClass.getDeclaredMethods();
            for (Method method : methods) {
                methodsList.add(method);
            }
        }

    }

    public String getResult() {
        String result = "";
        MyTest annotation;
        for (Method method : methodsList) {
            annotation = method.getAnnotation(MyTest.class);
            if(annotation == null){
                continue;
            }
            try {
                System.out.println(method.getName());


                method.invoke(method.getDeclaringClass().getDeclaredConstructor().newInstance());
                if(annotation.expected().equals(MyTest.None.class)){
                    result += method.getName() + "()" + " - " + "OK" + "\n";
                    continue;

                }else{
                    result += method.getName() + "()" + " - " + "FAILED" + "\n";
                    continue;
                }



        } catch (Exception e) {
               System.out.println(e.getCause());

                if(annotation.expected().isAssignableFrom(e.getCause().getClass())){
                    result += method.getName() + "()" + " - " + "OK" + "\n";
                    continue;
                }else{
                    result += method.getName() + "()" + " - " + "FAILED" + "\n";
                    continue;
                }

        }

        }
//        System.out.println(result);
        return result;


}
}
