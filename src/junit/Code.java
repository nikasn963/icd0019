package junit;

public class Code {

    public static boolean isSpecial(int candidate) {
        return candidate % 11 <= 3;
    }

    public static int longestStreak(String inputString) {
        if(inputString.length() == 0){
            return 0;
        }
        if(inputString == null){
            return 0;
        }
        int longest = 0;
        Character lastChar = null;
        int currentStreakLength = 0;
        for (Character currentChar: inputString.toCharArray()) {
            if (currentChar.equals(lastChar)){
                currentStreakLength++;
            }else{
                currentStreakLength = 1;
            }
            if (currentStreakLength > longest){
                longest = currentStreakLength;
            }
            lastChar = currentChar;
        }
//        int count = 1;
//        int bestCount = 0;
//        for(int i = 0; i < inputString.length()-1; i++){
//            if(inputString.charAt(i) == inputString.charAt(i+1)){
//                count++;
//            }
//
//            if (i == inputString.length()-2 && inputString.charAt(inputString.length()-1) == inputString.charAt(inputString.length()-2)){
//                count++;
//                if(count > bestCount){
//                    bestCount = count;
//                    count = 1;
//                }
//            }
//
//            if (inputString.charAt(i) != inputString.charAt(i+1) && count > bestCount){
//                    bestCount = count;
//                    count = 1;
//            }
//        }




        return longest;
    }

        public static Character mode(String inputString) {
        if(inputString == null){
            return null;
        }
        if(inputString.length() == 0){
            return null;
        }
        int count = 0;
        int bestCount = 0;
        int index = 0;
        for (int i = 0; i < inputString.length(); i++){
            if(count > bestCount){
                bestCount = count;
                index = i-1;
            }
            count = 0;
            for (int b = 0; b < inputString.length(); b++){
                if (inputString.charAt(i) == inputString.charAt(b)){
                    count++;
                }

            }
        }

        if(bestCount == 1) {
            return inputString.charAt(0);
        }


        return inputString.charAt(index);
    }

    public static int getCharacterCount(String allCharacters, char targetCharacter) {
        int count = 0;
        for(int i = 0; i < allCharacters.length(); i++){
            if(allCharacters.charAt(i) == targetCharacter){
                count++;
            }
        }
        return count;
    }

    public static int[] removeDuplicates(int[] integers) {
        int arrayLength = integers.length;
        int[] abi = new int[arrayLength];
        int actualLen = 0;
        int count;
        int checkForZero = 0;
        // Get first number into new array with same size
        abi[actualLen++] = integers[0];

        for(int i = 1; i < arrayLength; i++){ // target second value in given array
            count = 0;
            for(int b = 0; b < arrayLength; b++){ // do loop through all array
                if(integers[i] != abi[b]){
                    count++;
                }
                if(integers[i] == 0 && checkForZero < 1 ){ // here do force push first 0 to array ( after it is never working only for once)
                    abi[actualLen++] = integers[i];
                    checkForZero++;
                }

                if(integers[i] != abi[b] && count == arrayLength){ // checking if in new whole array no such value do append
                    abi[actualLen++] = integers[i];
                }
            }

        }

        int[] trueArr = getInts(abi, actualLen);


        return trueArr;
    }

    private static int[] getInts(int[] abi, int actualLen) {
        int[] trueArr =new int[actualLen];
        for(int i = 0; i < trueArr.length; i++){
            trueArr[i] = abi[i];

        }
        return trueArr;
    }

    public static int sumIgnoringDuplicates(int[] integers) {

        int[] trueArr = removeDuplicates(integers);


        int kokku = 0;
        for (int num:trueArr) {
            kokku += num;
        }
        return kokku;
    }


}
