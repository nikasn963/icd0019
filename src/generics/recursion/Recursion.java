package generics.recursion;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Recursion {

    public List<String> getParts(Path path) {
        List listPath = new ArrayList();
        Path runner = path;
        while(runner != null){
            listPath.add(runner.getFileName());
            runner = runner.getParent();
        }

        Collections.reverse(listPath);


        return listPath;
    }

    public List<String> getParts2(Path path) {

        if(path.getParent() != null){
            getParts2(path.getParent());

        }

        System.out.println(path.getFileName());


        return null;
    }

    public List<String> getParts3(Path path, List<String> parts) {

        // c)

        return null;
    }

    public List<String> getParts4(Path path) {

        // d)

        return null;
    }
}
