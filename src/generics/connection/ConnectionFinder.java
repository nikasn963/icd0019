package generics.connection;

import java.util.*;

public class ConnectionFinder<T extends Connection> {
    private final Map<T,Integer> connectionMap= new HashMap<>();
    private final List hasConnectionList = new ArrayList<>();
    private final List hasNotConnectionList= new ArrayList();
    private Integer check = 0;

    public void addAll(List<? extends T> connections) {
        for (T t : connections) {
            add(t);
        }
    }

    public void add(T connection) {
        connectionMap.put(connection, 1);
    }

    public boolean hasConnection(String a, String b) {
        for (T t : connectionMap.keySet()) {
            if(hasConnectionList.isEmpty()){
                for (T t1 : connectionMap.keySet()) {
                    hasConnectionList.add(t1.getFrom());
                    hasConnectionList.add(t1.getTo());
                }

            }else if(hasConnectionList.contains(t.getFrom())){
                hasConnectionList.add(t.getTo());
            }else {
                hasNotConnectionList.add(t.getFrom());
                hasNotConnectionList.add(t.getTo());
            }

        }

//        for (Object t : hasConnectionList) {
//            System.out.println(t);
//
//        }

        return hasConnectionList.contains(a) && hasConnectionList.contains(b);
//        for (T t : connectionMap.keySet()) {
//            if(t.getFrom() == a && t.getTo() == b || t.getFrom() == b && t.getTo() == a){
//                return true;
//            }
//        }
//        return false;
    }

    public List<String> findConnection(String a, String b) {
        throw new RuntimeException("not implemented yet");
    }
}
