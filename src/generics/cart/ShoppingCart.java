package generics.cart;

import test.LightOn;

import java.util.*;
import java.util.TreeMap;

public class ShoppingCart<T extends CartItem> {

    private final Map<T, Integer> itemsMap = new TreeMap<>(new Comparator<T>() {
        @Override
        public int compare(T o1, T o2) {
            return o1.getId().charAt(1) - o2.getId().charAt(1);
        }
    });

    private final List<Double> discountList = new ArrayList<>();


    public void add(T item) {
        if(itemsMap.containsKey(item)){
            itemsMap.put(item, itemsMap.get(item) + 1);
        }else{
            itemsMap.put(item, 1);
        }
    }

    public void removeById(String id) {
        for (T item : itemsMap.keySet()) {
            if(item.getId().equals(id)){
                itemsMap.remove(item);
            }
        }
    }

    public Double getTotal() {
        Double sum = 0.0;
        for (T item : itemsMap.keySet()) {
            sum += item.getPrice() * itemsMap.get(item) ;
        }
        if(!discountList.isEmpty()){
            for (Double discount : discountList) {
                sum = sum - (sum * discount / 100);
            }
        }
        return sum;
    }

    public void increaseQuantity(String id) {
        for (T t : itemsMap.keySet()) {
            if(t.getId().equals(id)){
                itemsMap.put(t, itemsMap.get(t)+1);
            }
        }
    }

    public void applyDiscountPercentage(Double discount) {
        discountList.add(discount);
    }

    public void removeLastDiscount() {
        discountList.remove(discountList.size() - 1);
    }

    public void addAll(List<? extends T> items) {
        for (T item : items) {
            add(item);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (T element: itemsMap.keySet()) {
            int amount = itemsMap.get(element);
            sb.append(String.format("(%s, %s, %s), ", element.getId(), element.getPrice(), amount));
        }
        int indexOfJunk = sb.lastIndexOf(",");
        sb.replace(indexOfJunk, sb.length(), "");
        return sb.toString();
    }


//        list.set(index, object);
//
//        list.set(index, list.get(index).function);

//

//    char arr[] = result.toCharArray();
//        if(arr[1] == 'C' && arr[2] == 'C' && arr[0] == 'C'){
//        result = "AAA";
//    }else if(arr[1] == 'C' && arr[2] == 'C'){
//        arr[0] = alphabet.get(alphabet.indexOf(arr[0]) + 1);
//        result = ("%s%s%s").formatted(arr[0],'A','A');
//    }else if(arr[2] == 'C'){
//        arr[1] = alphabet.get(alphabet.indexOf(arr[1]) + 1);
//        result = ("%s%s%s").formatted(arr[0],arr[1],'A');
//    }else{
//        arr[2] = alphabet.get(alphabet.indexOf(arr[2]) + 1);
//        result = ("%s%s%s").formatted(arr[0],arr[1],arr[2]);
    }


