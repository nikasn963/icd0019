//package exceptions.numbers;
//
//import java.io.FileInputStream;
//
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.nio.charset.StandardCharsets;
//
//import java.util.Properties;
//
//public class NumberConverter {
//    private Properties properties;
//    public NumberConverter(String lang) {
//
//        String filePath = "src/exceptions/numbers/numbers_" + lang + ".properties";
//
//        this.properties = new Properties();
//        FileInputStream is = null;
//
//        try {
//            is = new FileInputStream(filePath);
//
//            InputStreamReader reader = new InputStreamReader(
//                    is, StandardCharsets.UTF_8);
//
//            properties.load(reader);
//        } catch (IOException e) {
//            throw new MissingLanguageFileException(lang,e);
//        }catch (IllegalArgumentException e){
//            throw new BrokenLanguageFileException(lang,e);
//        } finally {
//            close(is);
//        }
//
//
//
//    }
//
//    private static void close(FileInputStream is) {
//        if (is == null) {
//            return;
//        }
//
//        try {
//            is.close();
//        } catch (IOException ignore) {
//
//        }
//    }
//
//
//
//    public String numberInWords(Integer number) {
//        int hundreds = number / 100;
//        String str = "";
//            if(hundreds > 0){
//                if(number % 100 == 0){
//                    return str + getKey(hundreds) + getKey("hundreds-before-delimiter") + getKey("hundred");
//                }
//                str += getKey(String.valueOf(hundreds)) +  getKey("hundreds-before-delimiter") + getKey("hundred") + getKey("hundreds-after-delimiter");
//                number = number - hundreds * 100;
//                if (number == 0){
//                    return str;
//                }
//            }
//            if(properties.containsKey(String.valueOf(number)) || number == 0){
//                return str + getKey(number);
//            }
//            int ones = number % 10;
//            if(number >= 10 && number < 20 ){
//                return str + (getKey(String.valueOf(ones)) + getKey("teen"));
//            }
//            int tens = number / 10 % 10;
//            if(number % 10 == 0){
//                return str + (getKey(String.valueOf(tens)) + getKey("tens-suffix"));
//            }
//            if(properties.containsKey(String.valueOf(number - ones))){
//                return str + getKey(String.valueOf(number - ones)) + getKey("tens-after-delimiter") + getKey(String.valueOf(ones));
//            }
//
//            str += getKey(String.valueOf(tens)) + getKey("tens-suffix") + getKey("tens-after-delimiter") + getKey(String.valueOf(ones));
//
//        return str;
//
//    }
//
//    private String getKey(Object number) {
//        if(!properties.containsKey(String.valueOf(number))){
//
//            throw new MissingTranslationException(String.valueOf(number));
//        }
//        return properties.getProperty(String.valueOf(number));
//    }
//}


package exceptions.numbers;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class NumberConverter {
    Properties properties = new Properties();
    public NumberConverter(String lang) {
        String filePath = String.format("src/exceptions/numbers/numbers_%s.properties", lang);


        FileInputStream is = null;

        try {
            is = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader(
                    is, StandardCharsets.UTF_8);

            properties.load(reader);

        } catch (IllegalArgumentException e) {
            throw new BrokenLanguageFileException(lang, e);
        } catch (Exception e) {
            throw new MissingLanguageFileException(lang, e);
        } finally {
            close(is);
        }

    }

    private static void close(FileInputStream is) {
        if (is == null) {
            return;
        }

        try {
            is.close();
        } catch (IOException ignore) {}
    }

    public String numberInWords(Integer number) {
        System.out.println("NUMBER START " + number);

        if (properties.isEmpty()) {
            throw new MissingTranslationException(number.toString());
        }

        String inWord = "";
        if (properties.containsKey(number.toString())) {
            System.out.println("IF in PROP");
            inWord += properties.getProperty(number.toString());
            System.out.println("");
            return inWord;
        }
        if (String.valueOf(number).length() == 3) {
            System.out.println("IS HUNDRED");
            inWord += hundresdInWords(number);
        } else {
            System.out.println("in else IS TENTH");
            inWord += tenthInWords(number);
        }



        System.out.println("inWord " + inWord);
        System.out.println("");
        return inWord;
    }

    public String hundresdInWords(Integer number) {
        String inWord = "";
        int sajased = number / 100;
        if (number % 100 == 0) {
            inWord += properties.getProperty(String.valueOf(sajased))
                    + properties.getProperty("hundreds-before-delimiter")
                    + properties.getProperty("hundred");
            System.out.println("100 IF1");
        } else {
            inWord += properties.getProperty(String.valueOf(sajased))
                    + properties.getProperty("hundreds-before-delimiter")
                    + properties.getProperty("hundred")
                    + properties.getProperty("hundreds-after-delimiter")
                    + numberInWords(number % 100);
//            System.out.println(numberInWords(number % 100));
//            System.out.println("hundresdInWords " + inWord);
//            System.out.println("100 ELSE");
        }

        return inWord;
    }

    public String tenthInWords(Integer number) {
        String inWord = "";
        int kumnesed = number / 10;
        int taiskumme = number / 10 * 10;

        if (number / 10 == 1) {
            System.out.println("10 IF1");
            inWord += properties.getProperty(String.valueOf(number % 10))
                    + properties.getProperty("teen");
        } else if (number % 10 == 0) {
            System.out.println("10 nulliga");
            number = number / 10;
            inWord += properties.getProperty(number.toString())
                    + properties.getProperty("tens-suffix");
        } else if (properties.containsKey(String.valueOf(taiskumme)) && (number % 10) > 0) {
            int uhesed = number % 10;
            inWord += properties.getProperty(String.valueOf(taiskumme))
                    + properties.getProperty("tens-after-delimiter")
                    + properties.getProperty(String.valueOf(uhesed));
        } else if (!(properties.containsKey(number.toString())) && (number % 10) > 0) {
            System.out.println("10 IF3");
            int uhesed = number % 10;
            number = number / 10;
            inWord += properties.getProperty(number.toString())
                    + properties.getProperty("tens-suffix")
                    + properties.getProperty("tens-after-delimiter")
                    + properties.getProperty(String.valueOf(uhesed));
        }  else {
            System.out.println("10 ELSE");
            inWord += properties.getProperty(String.valueOf(kumnesed))
                    + " "
                    + properties.getProperty("tens-suffix");
        }
        return inWord;
    }

    public String onesInWords(Integer number) {
        String inWord = "";
        int uhesed = number % 10;
        System.out.println("in ones");
        inWord += properties.getProperty(String.valueOf(uhesed));
        return inWord;
    }
}



