package types;

import java.util.Arrays;
import java.util.Random;

public class Code {

    public static void main(String[] args) {

        int[] numbers = {1, 3, -2, 9};
//        numbers = new int[] {};

        System.out.println(sum(numbers)); // 11
        System.out.println(average(numbers));
        System.out.println(minimumElement(numbers));
        System.out.println(minimumElement(numbers));
        System.out.println(asString(numbers));
        System.out.println(mode("abbcd"));
        System.out.println(squareDigits("g2h4"));
    }

    public static int sum(int[] numbers) {
        int kokku = 0;
        for (int number: numbers)
        {
            kokku += number;

        }


        return kokku;
    }

    public static double average(int[] numbers) {
        double kokku = 0;
        int count = 0;
        for (int num: numbers) {
            kokku += Double.valueOf(num);
            count++;
        }
        return kokku / Double.valueOf(count);
    }

    public static Integer minimumElement(int[] integers) {
        if (integers.length == 0){
            return null;
        }
        int min = integers[0];
        for (int each:integers) {
            if(each < min){
                min = each;
            }

        }
        return min;
    }

    public static String asString(int[] elements) {
        String result = "";
        for (int i = 0; i<elements.length; i++){
            if(i == elements.length-1){
                result += String.valueOf(elements[i]);
            }else{
                result += String.valueOf(elements[i]) + ", ";
            }
        }
        return result;
    }

    public static Character mode(String input) {
        if(input.length() == 0){
            return null;
        }
        int count = 0;
        int bestCount = 0;
        int index = 0;
        for (int i = 0; i < input.length(); i++){
            if(count > bestCount){
                bestCount = count;
                index = i-1;
            }
            count = 0;
            for (int b = 1; b < input.length(); b++){
                if (input.charAt(i) == input.charAt(b)){
                    count++;
                }

            }
        }

        return input.charAt(index);
    }

    public static String squareDigits(String s) {
        char[] ch=s.toCharArray();
        int temp;
        String result = "";
        for (char c : ch) {
            if (Character.isDigit(c)) {
                temp = Integer.parseInt(Character.toString(c)) * Integer.parseInt(Character.toString(c));
                result += temp;

            } else {
                result += Character.toString(c);
            }
        }
        return result;
    }

    public static int isolatedSquareCount() {
        boolean[][] matrix = getSampleMatrix();

        printMatrix(matrix);

        int isolatedCount = 0;

        // count isolates squares here

        return isolatedCount;
    }

    private static void printMatrix(boolean[][] matrix) {
        for (boolean[] row : matrix) {
            System.out.println(Arrays.toString(row));
        }
    }

    private static boolean[][] getSampleMatrix() {
        boolean[][] matrix = new boolean[10][10];

        Random r = new Random(5);
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                matrix[i][j] = r.nextInt(5) < 2;
            }
        }

        return matrix;
    }
}
