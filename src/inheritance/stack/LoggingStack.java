package inheritance.stack;

import java.util.Stack;

public class LoggingStack extends Stack<Integer> {
    @Override
    public Integer push(Integer item) {
        System.out.println("Do push " + item + "number");
        return super.push(item);
    }

    @Override
    public synchronized Integer pop() {
        Integer pop = super.pop();
        System.out.println("Popping that " + pop);
        return pop;
    }

    public void pushAll(Integer... numbers){
        for (Integer number: numbers) {
            push(number);
        }
    }
}
