package inheritance.calc;

public class TaxFreePayCalculator extends PayCalculator {
    public static final Double TAX_RATE = 0.0;
    @Override
    protected Double getTaxRate() {
        return TAX_RATE;
    }
}
