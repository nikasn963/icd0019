package collections.streaks;

import java.util.ArrayList;
import java.util.List;

public class Code {

    public static List<List<String>> getStreakList(String input) {
        List<List<String>> streaks = new ArrayList<>();
        ArrayList<String> currentStreak = new ArrayList<>();
        for (String currentSymbol : input.split("")) {
            if(input == "" || input == null){
                return streaks;
            }
            if (currentStreak.isEmpty() || currentStreak.contains(currentSymbol)) {
                currentStreak.add(currentSymbol);
            } else {
                streaks.add(new ArrayList<>(currentStreak));
                currentStreak.removeAll(currentStreak);
                currentStreak.add(currentSymbol);
            }
        }
        streaks.add(new ArrayList<>(currentStreak));
        return streaks;
    }

    





}
