package collections.set;

import collections.benchmark.MySet;
import org.junit.Test;

import java.util.*;


public class Birthday {

    @Test
    public void runCode() {
        List<Integer> counts = new ArrayList<>();
        for (int i = 0; i< 1000; i++){
            counts.add(firstCollision());
        }
        int sum= 0;
        for (Integer count : counts) {
            sum += count;

        }
        System.out.println(counts.size());
        System.out.println(sum);
        System.out.println(sum / counts.size());


    }

    private int firstCollision() {
        Random r = new Random();
        Set<Integer> birhtdayDateList = new HashSet<>(); // kui see panna üleval

        for (int i = 0; i < 365; i++) {
            int randomDayOfYear = r.nextInt(365);
            if (birhtdayDateList.contains(randomDayOfYear)) {
                return i;
            } else {
                birhtdayDateList.add(randomDayOfYear);
            }
        }
        throw new IllegalStateException("should not be here");
    }
}