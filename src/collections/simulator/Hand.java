package collections.simulator;

import java.util.*;

public class Hand implements Iterable<Card>, Comparable<Hand> {

    private List<Card> cards = new ArrayList<>();

    public void addCard(Card card) {
        cards.add(card);
    }

    @Override
    public String toString() {
        return cards.toString();
    }

    public HandType getHandType() {


        if(getFullHouse() == 7){
            return HandType.FULL_HOUSE;
        }

        if (getFlash() == 6){
            return HandType.STRAIGHT_FLUSH;
        }
        if(getFlash() == 5){
            return HandType.FLUSH;
        }
        if(getStraightAce() == 3){
            return HandType.STRAIGHT;
        }
        if(getStraight() == 4){
            return HandType.STRAIGHT;
        }
        if(getTrips() == 3){
            return HandType.TRIPS;
        }
        int num = 0;
        num = getNum(num);


        switch (num) {
            case 1:
                return HandType.ONE_PAIR;
            case 2:
                return HandType.TWO_PAIRS;
            default: return HandType.FOUR_OF_A_KIND;
        }
    }

    private int getNum(int num) {
        for (int i = 0; i < cards.size()-1; i++){

            if(cards.get(i).getValue() == cards.get(i+1).getValue()){
                num++;
            }
        }
        return num;
    }

    private int getFullHouse() {
        int amount1 = 0;
        int breakPoint = 0;

        List<Card> tempCards = new ArrayList<>(cards);
        int loopSize = tempCards.size();
        Collections.sort(tempCards);

        Card.CardValue currentCard = tempCards.get(0).getValue();
        for(int i = 0; i < loopSize; i++){
            if (currentCard == tempCards.get(i).getValue()) {
                amount1++;
                currentCard = tempCards.get(i).getValue();
                breakPoint = i;
            }else {break;}
        }
        try {
            currentCard = tempCards.get(breakPoint+1).getValue();
        } catch (Exception e) {
            return 0;
        }
        breakPoint++;
        int amount2 = 0;
            for(int i = breakPoint; i < loopSize; i++){
                if (currentCard == tempCards.get(i).getValue()) {
                    amount2++;
                    currentCard = tempCards.get(i).getValue();
                }else {break;}
            }
        if(amount1 == 2 && amount2 == 3 || amount2 == 2 && amount1 == 3){
            return 7;
        }


        return 0;
    }

    private int getFlash() {
        int amount = 0;
        int straightFlush = 0;
        Card.CardSuit currentCardSuit = cards.get(0).getSuit();
        Card.CardValue currentCardValue = cards.get(0).getValue();
        for (Card card : cards) {
            if(currentCardSuit == card.getSuit()){
                if(currentCardValue.compareTo(card.getValue()) == -1){
                    straightFlush++;
                    currentCardValue = card.getValue();
                }
                amount++;
                currentCardSuit = card.getSuit();
            }else {
                amount = 0;
            }
        }

        if(straightFlush == 4){
            return 6;
        }

        return amount;
    }

    private int getStraightAce() {
        int straight = 0;
        if (cards.get(0).getValue() == Card.CardValue.A) {
            for (int i = 1; i < cards.size()-1; i++){
                if(cards.get(i).compareTo(cards.get(i+1)) == -1){
                    straight++;
                }
            }
        }
        return straight;
    }

    private int getStraight() {
        int straight = 0;
        for (int i = 0; i < cards.size()-1; i++){
            if(cards.get(i).compareTo(cards.get(i+1)) == -1){
                straight++;
            }
        }
        return straight;
    }

    private int getTrips() {
        int a = 0;
        Card.CardValue currentCard = cards.get(0).getValue();
        for (Card card : cards) {
            if(currentCard == card.getValue()){
                a++;
                currentCard = card.getValue();
            }else {
                a = 0;
            }
        }
        
        return a;
    }

    public boolean contains(Card card) {
        return cards.contains(card);
    }

    public boolean isEmpty() {
        return cards.isEmpty();
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }

    @Override
    public int compareTo(Hand other) {
        return 0;
    }
}
