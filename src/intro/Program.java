package intro;

public class Program {

    public static void main(String[] args) {
        int decimal = asDecimal("11001101");

        System.out.println(decimal); // 205
        System.out.println(asString((decimal)));
    }

    public static String asString(int input) {
        int tempJaak = 0;
        int devisionResult = input;
        String result = "";
        while(true){
            tempJaak = devisionResult % 2;
            devisionResult =  devisionResult / 2;
            result = Integer.toString(tempJaak) + result;
            if(devisionResult < 1){
                break;
            }
        }
        return result;
    }

    public static int asDecimal(String input) {
        int temp = 0;
        int stringLength = input.length();
        int powerNumber = stringLength-1;
        for (int i = 0; i < stringLength; i++) {
            int parsedNumber = Integer.parseInt(String.valueOf(input.charAt(i)));
            temp = temp + (parsedNumber * pow(2, powerNumber));
            powerNumber--;
        }
        return temp;
    }

    private static int pow(int arg, int power) {
        int temp = 1;
        for (int i = 0; i < power; i++) {
            temp = temp*arg;
            
        }

        return temp;
    }
}
