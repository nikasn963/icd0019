package poly.undo;

import java.util.ArrayList;
import java.util.List;

public class Calculator<T> {

    private double value;

    List<String> list = new ArrayList<>();

    public void input(double value) {
        this.value = value;
        list.add(String.format("insert %s", value));
    }

    public void add(double addend) {
        value += addend;
        list.add(String.format("added %s", addend));
    }

    public void multiply(double multiplier) {
        value *= multiplier;
        list.add(String.format("multiplied %s", multiplier));
    }

    public double getResult() {
        return value;
    }

    public void undo() {
       String lastElement = list.get(list.size() - 1);
       if(lastElement.contains("insert")){
           value = 0;
           System.out.println(value);
           list.remove(list.size() - 1);
       }else if(lastElement.contains("added")){
           String[] splitedText = lastElement.split(" ");
           value = value - Double.valueOf(splitedText[1]);
           System.out.println(value);
           list.remove(list.size() - 1);

       }else if(lastElement.contains("multiplied")){
           String[] splitedText = lastElement.split(" ");
           value = value / Double.valueOf(splitedText[1]);
           System.out.println(value);
           list.remove(list.size() - 1);
       }
    }
}
