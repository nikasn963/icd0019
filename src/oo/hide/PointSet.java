package oo.hide;

import java.util.Objects;

public class PointSet {

    private Point[] points;
    private int elementsInArrayIndex = 0;
    private int count = 0;
    private int capacity;
    public PointSet(int capacity) {
        this.capacity = capacity;
        points = new Point[capacity];
    }
    public PointSet() {

        this(10);
    }


    @Override
    public boolean equals(Object obj) {

        PointSet other = (PointSet) obj;
        count = 0;
        for (Point point1: points) {
            for (Point point2 : other.points) {
                if(point1 != null && point2 != null && Objects.equals(point1.x, point2.x) && Objects.equals(point1.y, point2.y)){
                    System.out.println(point2);
                    count++;
                }
            }

        }
        if(elementsInArrayIndex != 0 && other.elementsInArrayIndex == 0){
            return false;
        }
        if(elementsInArrayIndex == 0 && other.elementsInArrayIndex == 0){
            return true;
        }
        return count == other.elementsInArrayIndex;
    }


    @Override
    public String toString() {
        String str = "";
        for(int i = 0; i< elementsInArrayIndex-1; i++) {
            str +="(%s, %s), ".formatted(points[i].x, points[i].y);
        }
        str += "(%s, %s)".formatted(points[elementsInArrayIndex-1].x, points[elementsInArrayIndex-1].y);
        return str;
    }





    public void add(Point point) {
        if (elementsInArrayIndex == capacity) {

            // Creating a new array double the size
            // of array declared above
            PointSet newArr = new PointSet(2 * capacity);

            // Iterating over new array using for loop
            for (int i = 0; i < capacity; i++) {
                newArr.points[i] = points[i];
            }

            // Assigning new array to original array
            // created above
            points = newArr.points;
        }
        count = 0;
        for (Point value : points) {
            if (point.equals(value) && points != null) {
                count++;
            }
        }


        if(count == 0){
            System.out.println(elementsInArrayIndex);
            points[elementsInArrayIndex++] = point;

        }

        for (Point point1:points) {
            System.out.println(point1 + " " + "point");
        }




    }

    public int size() {
        return elementsInArrayIndex;
    }

    public boolean contains(Point point) {
        count = 0;
        for (Point value : points) {
            if (point.equals(value) && points != null) {
                count++;
            }
        }


        return count > 0;
    }

    public PointSet subtract(PointSet other) {
        PointSet duplicates = new PointSet();
        PointSet trueArray = new PointSet();

        for (Point point1:other.points) {

            for (Point point2: points) {
                if(point1 != null && point2 != null && !(Objects.equals(point1.x, point2.x) && Objects.equals(point1.y, point2.y))){
                    System.out.println(point2 + " " + "point2");
                    duplicates.add(point2);
                }

            }

        }
        extracted(duplicates, trueArray);

        return trueArray;
    }

    private void extracted(PointSet duplicates, PointSet trueArray) {
        for(int i = 0; i < duplicates.elementsInArrayIndex; i++){
            if(count == 1){
                trueArray.add(duplicates.points[i-1]);
            }
            count = 0;
            for(int b = 0; b < duplicates.elementsInArrayIndex; b++){
                if(duplicates.points[i] == duplicates.points[b]){
                    count++;
                }

            }
        }
    }

    public PointSet intersect(PointSet other) {
        PointSet duplicates = new PointSet();

        for (Point point1:other.points) {
            for (Point point2: points) {
                if(point1 != null && point2 != null && Objects.equals(point1.x, point2.x) && Objects.equals(point1.y, point2.y)){
                    System.out.println(point2 + " " + "point2");
                    duplicates.add(point2);
                }

            }

        }
        return duplicates;
    }
}
