package oo.hide;

public class Fibonacci {

    private int current = 0;
    private int next = 1;
    public int nextValue() {
        int currentValue = current;
        current = next;
        next = currentValue + next;

        return currentValue;
    }

}
