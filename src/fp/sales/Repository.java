package fp.sales;



import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import java.util.List;

public class Repository {

    private static final String FILE_PATH = "src/fp/sales/sales-data.csv";

    private DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("dd.MM.yyyy");

    private Entry arrayToEntry(String[] array){
        Entry entry = new Entry();
            entry.setDate(LocalDate.parse(array[0], formatter));
            entry.setState(array[1]);
            entry.setProductId(array[2]);
            entry.setCategory(array[3]);
            entry.setAmount(Double.valueOf(array[5].replace(",",".")));
        return entry;
    }


    public List<Entry> getEntries() {

        // reads lines form the file and creates entry objects for each line.

        List<String[]> lines = new ArrayList<>();
        List<Entry> result;

//        try {
//            lines =  Files.readAllLines(Path.of(FILE_PATH)).stream().map(line -> line.split("\t")).toList();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
////        System.out.println(Arrays.stream(lines.get(0)).toList());
//        for (String[] line : lines) {
//            result.add(new Entry(line));
//        }

//        try {
//            lines = Files.readAllLines(Path.of(FILE_PATH));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }finally {
//            for (String line : lines) {
//                System.out.println(line);
//            }
//        }

        try {
           lines = Files.readAllLines(Path.of(FILE_PATH)).stream().skip(1).map(line -> line.split("\t")).toList();


        } catch (IOException e) {

        }


        for (String[] line : lines) {
//            System.out.println(Arrays.stream(line).toList());
        }
        result = lines.stream().map(x -> arrayToEntry(x)).toList();

        return result;










    }



}


