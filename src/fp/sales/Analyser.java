package fp.sales;



import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Analyser {

    private Repository repository;


    public Analyser(Repository repository) {
        this.repository = repository;
    }

    public Double getTotalSales() {
        double sum = repository.getEntries().stream()
                .mapToDouble(x -> x.getAmount())
                .sum();
        return sum;
    }

    public Double getSalesByCategory(String category) {
        double sum = repository.getEntries().stream()
                .filter(x -> Objects.equals(x.getCategory(), "Office Supplies"))
                .mapToDouble(x -> x.getAmount())
                .sum();
        return sum;
    }

    public Double getSalesBetween(LocalDate start, LocalDate end) {
        double sum = repository.getEntries().stream()
                    .filter(x -> x.getDate().isAfter(start) && x.getDate().isBefore(end))
                .mapToDouble(x -> x.getAmount())
                .sum();
        return sum;
    }

    public String mostExpensiveItems() {
        List<Entry> entry = repository.getEntries().stream().sorted(Collections.reverseOrder(Comparator.comparing(x -> x.getAmount()))).toList();
        List<Entry> entry1 = entry.stream().limit(3).sorted(Comparator.comparing(x -> x.getProductId())).toList();
        return entry1.stream().map(each -> each.getProductId()).collect(Collectors.joining(", "));

    }

    public String statesWithBiggestSales() {
        var map = repository.getEntries().stream().collect(
                Collectors.toMap(
                        each -> each.getState(), // function that produces a key (from each item)
                        each -> each.getAmount(), // function that produces a value
                        (a, b) -> a + b));


        return map.entrySet().stream().sorted((a,b) -> b.getValue().compareTo(a.getValue())).limit(3)
                .map(e -> e.getKey()).collect(Collectors.joining(", "));
    }
}
